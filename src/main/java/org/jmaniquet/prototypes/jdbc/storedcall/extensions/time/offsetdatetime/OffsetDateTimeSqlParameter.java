package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.offsetdatetime;

import java.sql.Types;

import org.springframework.jdbc.core.SqlParameter;

public class OffsetDateTimeSqlParameter extends SqlParameter {

	public OffsetDateTimeSqlParameter(String name) {
		super(name, Types.TIMESTAMP_WITH_TIMEZONE);
	}
}
