package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdate;

import java.sql.Types;

import org.springframework.jdbc.core.SqlInOutParameter;

public class LocalDateSqlInOutParameter extends SqlInOutParameter {

	public LocalDateSqlInOutParameter(String name) {
		super(name, Types.DATE, null, new LocalDateSqlReturnType());
	}
}
