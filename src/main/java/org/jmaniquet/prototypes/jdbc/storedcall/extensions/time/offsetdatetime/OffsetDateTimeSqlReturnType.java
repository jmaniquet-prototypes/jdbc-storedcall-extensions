package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.offsetdatetime;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.time.OffsetDateTime;

import org.springframework.jdbc.core.SqlReturnType;

class OffsetDateTimeSqlReturnType implements SqlReturnType {

	@Override
	public Object getTypeValue(CallableStatement cs, int paramIndex, int sqlType, String typeName) throws SQLException {
		return cs.getObject(paramIndex, OffsetDateTime.class);
	}
}
