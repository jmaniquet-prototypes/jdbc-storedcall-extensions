package org.jmaniquet.prototypes.jdbc.storedcall.extensions.array;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.springframework.dao.CleanupFailureDataAccessException;
import org.springframework.jdbc.support.SqlValue;

/**
 * If your vendor does not support {@link java.sql.Connection#createArrayOf Connection.createArrayOf},<br/>
 * you will need to provide an alternate implementation of {@link ArrayCreatorCallback}.<br/>
 * 
 * @author Jmaniquet
 * @param <T>
 */
public class ArraySqlValue<T> implements SqlValue {
	
	private String arrayType;
	private Collection<T> values;
	
	// Standard JDBC way of creating sql Array
	private ArrayCreatorCallback arrayCreatorCallback = (con, typeName, elements) -> con.createArrayOf(typeName, elements);
	
	private Array forCleanup;

	@SafeVarargs
	public ArraySqlValue(String arrayType, T ... values) {
		this(arrayType, List.of(values));
	}
	
	public ArraySqlValue(String arrayType, Collection<T> values) {
		this.arrayType = arrayType;
		this.values = values;
	}

	@Override
	public void setValue(PreparedStatement ps, int paramIndex) throws SQLException {
		Array sqlArray = this.arrayCreatorCallback.createArrayOf(ps.getConnection(), arrayType, values.toArray());
		ps.setArray(paramIndex, sqlArray);
		this.forCleanup = sqlArray;
	}

	@Override
	public void cleanup() {
		if (this.forCleanup != null) {
			try {
				this.forCleanup.free();
			} catch (SQLException e) {
				throw new CleanupFailureDataAccessException("Error when freeing array", e);
			}
		}
	}
	
	public ArraySqlValue<T> withArrayCreatorCallback(ArrayCreatorCallback arrayCreatorCallback) {
		this.arrayCreatorCallback = arrayCreatorCallback;
		return this;
	}
}
