package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Struct;
import java.sql.Types;

import org.springframework.jdbc.support.SqlValue;

public class StructSqlValue<T> implements SqlValue {
	
	private T value;
	private StructMapper<T> mapper;

	public StructSqlValue(T value, StructMapper<T> mapper) {
		this.value = value;
		this.mapper = mapper;
	}

	@Override
	public void setValue(PreparedStatement ps, int paramIndex) throws SQLException {
		Struct struct = mapper.toStruct(this.value, ps.getConnection());
		ps.setObject(paramIndex, struct, Types.STRUCT);
	}

	@Override
	public void cleanup() {}
}
