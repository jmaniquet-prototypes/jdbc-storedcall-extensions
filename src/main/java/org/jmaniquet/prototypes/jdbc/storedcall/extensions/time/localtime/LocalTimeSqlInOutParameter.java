package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localtime;

import java.sql.Types;

import org.springframework.jdbc.core.SqlInOutParameter;

public class LocalTimeSqlInOutParameter extends SqlInOutParameter {
	
	public LocalTimeSqlInOutParameter(String name) {
		this(name, Types.TIME);
	}
	
	private LocalTimeSqlInOutParameter(String name, int sqlType) {
		super(name, sqlType, null, new LocalTimeSqlReturnType());
	}
	
	/**
	 * Oracle seems bugged when using Types.TIME : fractional seconds are lost.<br/>
	 * The work-around I found is to use Types.TIMESTAMP.<br/>
	 * For semantic purpose, I chose to wrap it in a dedicated factory method.<br/>
	 * 
	 * See <a href="https://stackoverflow.com/questions/61840178/persisting-java-time-localtime-into-oracle-with-types-time-loses-fractional-seco">this stackoverflow question.</a>
	 */
	public static LocalTimeSqlInOutParameter forOracle(String name) {
		return new LocalTimeSqlInOutParameter(name, Types.TIMESTAMP);
	}
}
