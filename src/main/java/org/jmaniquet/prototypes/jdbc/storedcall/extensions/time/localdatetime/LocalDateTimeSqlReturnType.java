package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdatetime;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.springframework.jdbc.core.SqlReturnType;

class LocalDateTimeSqlReturnType implements SqlReturnType {

	@Override
	public Object getTypeValue(CallableStatement cs, int paramIndex, int sqlType, String typeName) throws SQLException {
		return cs.getObject(paramIndex, LocalDateTime.class);
	}
}
