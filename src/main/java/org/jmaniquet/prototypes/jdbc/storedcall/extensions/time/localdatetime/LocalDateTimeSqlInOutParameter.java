package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdatetime;

import java.sql.Types;

import org.springframework.jdbc.core.SqlInOutParameter;

public class LocalDateTimeSqlInOutParameter extends SqlInOutParameter {

	public LocalDateTimeSqlInOutParameter(String name) {
		super(name, Types.TIMESTAMP, null, new LocalDateTimeSqlReturnType());
	}
}
