package org.jmaniquet.prototypes.jdbc.storedcall.extensions.array;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.SqlReturnType;

class ArraySqlReturnType<T> implements SqlReturnType {

	@Override
	public Object getTypeValue(CallableStatement cs, int paramIndex, int sqlType, String typeName) throws SQLException {
		Array sqlArray = cs.getArray(paramIndex);
		Object[] objects = (Object[]) sqlArray.getArray();
		return List.of(objects);
	}
}
