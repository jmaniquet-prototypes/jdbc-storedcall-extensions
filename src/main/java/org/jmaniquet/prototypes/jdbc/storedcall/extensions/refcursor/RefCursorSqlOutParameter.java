package org.jmaniquet.prototypes.jdbc.storedcall.extensions.refcursor;

import java.sql.Types;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;

public class RefCursorSqlOutParameter<T> extends SqlOutParameter {

	public RefCursorSqlOutParameter(String name, RowMapper<T> rowMapper) {
		super(name, Types.REF_CURSOR, rowMapper);
	}
}
