package org.jmaniquet.prototypes.jdbc.storedcall.extensions.oracle;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;

import org.jmaniquet.prototypes.jdbc.storedcall.extensions.array.ArrayCreatorCallback;

import oracle.jdbc.OracleConnection;

public class OracleArrayCreatorCallback implements ArrayCreatorCallback {

	@Override
	public Array createArrayOf(Connection con, String typeName, Object[] elements) throws SQLException {
		OracleConnection unwrap = con.unwrap(OracleConnection.class);
		return unwrap.createARRAY(typeName, elements);
	}
}
