package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Struct;

public interface StructMapper<T> {

	Struct toStruct(T source, Connection conn) throws SQLException;
	
	T fromStruct(Struct struct) throws SQLException;
}
