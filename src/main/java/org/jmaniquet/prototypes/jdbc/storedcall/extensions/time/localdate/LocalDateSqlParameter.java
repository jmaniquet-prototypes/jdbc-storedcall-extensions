package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdate;

import java.sql.Types;

import org.springframework.jdbc.core.SqlParameter;

public class LocalDateSqlParameter extends SqlParameter {

	public LocalDateSqlParameter(String name) {
		super(name, Types.DATE);
	}
}
