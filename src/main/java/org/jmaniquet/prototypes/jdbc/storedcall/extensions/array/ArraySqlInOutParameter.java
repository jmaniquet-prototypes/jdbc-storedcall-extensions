package org.jmaniquet.prototypes.jdbc.storedcall.extensions.array;

import java.sql.Types;

import org.springframework.jdbc.core.SqlInOutParameter;

public class ArraySqlInOutParameter<T> extends SqlInOutParameter {
	
	public ArraySqlInOutParameter(String name, String typeName) {
		super(name, Types.ARRAY, typeName, new ArraySqlReturnType<T>());
	}
}
