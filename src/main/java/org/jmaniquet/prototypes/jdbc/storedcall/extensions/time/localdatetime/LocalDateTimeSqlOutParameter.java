package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdatetime;

import java.sql.Types;

import org.springframework.jdbc.core.SqlOutParameter;

public class LocalDateTimeSqlOutParameter extends SqlOutParameter {

	public LocalDateTimeSqlOutParameter(String name) {
		super(name, Types.TIMESTAMP, null, new LocalDateTimeSqlReturnType());
	}
}
