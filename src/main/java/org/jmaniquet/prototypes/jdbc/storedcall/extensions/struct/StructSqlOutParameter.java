package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import java.sql.Types;

import org.springframework.jdbc.core.SqlOutParameter;

public class StructSqlOutParameter<T> extends SqlOutParameter {

	public StructSqlOutParameter(String name, String typeName, StructMapper<T> structMapper) {
		super(name, Types.STRUCT, typeName, new StructSqlReturnType<T>(structMapper));
	}
}
