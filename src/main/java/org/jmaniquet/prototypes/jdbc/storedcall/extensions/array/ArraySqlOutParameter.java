package org.jmaniquet.prototypes.jdbc.storedcall.extensions.array;

import java.sql.Types;

import org.springframework.jdbc.core.SqlOutParameter;

public class ArraySqlOutParameter<T> extends SqlOutParameter {
	
	public ArraySqlOutParameter(String name, String typeName) {
		super(name, Types.ARRAY, typeName, new ArraySqlReturnType<T>());
	}
}
