package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdatetime;

import java.sql.Types;

import org.springframework.jdbc.core.SqlParameter;

public class LocalDateTimeSqlParameter extends SqlParameter {

	public LocalDateTimeSqlParameter(String name) {
		super(name, Types.TIMESTAMP);
	}
}
