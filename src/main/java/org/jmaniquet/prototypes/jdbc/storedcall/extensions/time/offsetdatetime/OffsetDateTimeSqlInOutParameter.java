package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.offsetdatetime;

import java.sql.Types;

import org.springframework.jdbc.core.SqlInOutParameter;

public class OffsetDateTimeSqlInOutParameter extends SqlInOutParameter {

	public OffsetDateTimeSqlInOutParameter(String name) {
		super(name, Types.TIMESTAMP_WITH_TIMEZONE, null, new OffsetDateTimeSqlReturnType());
	}
}
