package org.jmaniquet.prototypes.jdbc.storedcall.extensions.array;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Callback interface for creating instances of {@link java.sql.Array Array}<br/>
 * Provide your own implementation if your vendor does not support {@link java.sql.Connection#createArrayOf Connection.createArrayOf}.
 * @author Jmaniquet
 */
@FunctionalInterface
public interface ArrayCreatorCallback {

	Array createArrayOf(Connection con, String typeName, Object[] elements) throws SQLException;
}
