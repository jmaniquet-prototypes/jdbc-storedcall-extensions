package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.offsetdatetime;

import java.sql.Types;

import org.springframework.jdbc.core.SqlOutParameter;

public class OffsetDateTimeSqlOutParameter extends SqlOutParameter {

	public OffsetDateTimeSqlOutParameter(String name) {
		super(name, Types.TIMESTAMP_WITH_TIMEZONE, null, new OffsetDateTimeSqlReturnType());
	}
}
