package org.jmaniquet.prototypes.jdbc.storedcall.extensions.array;

import java.sql.Types;

import org.springframework.jdbc.core.SqlParameter;

public class ArraySqlParameter extends SqlParameter {

	public ArraySqlParameter(String name, String typeName) {
		super(name, Types.ARRAY, typeName);
	}
}
