package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localtime;

import java.sql.Types;

import org.springframework.jdbc.core.SqlOutParameter;

public class LocalTimeSqlOutParameter extends SqlOutParameter {
	
	public LocalTimeSqlOutParameter(String name) {
		this(name, Types.TIME);
	}
	
	private LocalTimeSqlOutParameter(String name, int sqlType) {
		super(name, sqlType, null, new LocalTimeSqlReturnType());
	}
	
	/**
	 * Oracle seems bugged when using Types.TIME : fractional seconds are lost.<br/>
	 * The work-around I found is to use Types.TIMESTAMP.<br/>
	 * For semantic purpose, I chose to wrap it in a dedicated factory method.<br/>
	 * 
	 * See <a href="https://stackoverflow.com/questions/61840178/persisting-java-time-localtime-into-oracle-with-types-time-loses-fractional-seco">this stackoverflow question.</a>
	 */
	public static LocalTimeSqlOutParameter forOracle(String name) {
		return new LocalTimeSqlOutParameter(name, Types.TIMESTAMP);
	}
}
