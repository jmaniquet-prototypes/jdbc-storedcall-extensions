package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import java.sql.SQLException;
import java.sql.Struct;
import java.util.stream.Stream;

/**
 * Handles common defensive code for null and the checking of the size of the struct.getAttributes array<br/>
 * If you don't need it implements {@link StructMapper} directly.<br/>
 * @author Jmaniquet
 * @param <T>
 */
public interface StructAttributesMapper<T> extends StructMapper<T> {

	default T fromStruct(Struct struct) throws SQLException {
		if (struct == null) {
			return null;
		}

		Object[] attributes = struct.getAttributes();
		onUnexpectedStructSizeThenThrowException(expectedStructSize(), struct.getSQLTypeName(), attributes);
		return fromStructAttributes(attributes);
	}
	
	T fromStructAttributes(Object[] attributes) throws SQLException;
	
	/**
	 * Expected length of the struct.getAttributes array for a given mapping
	 */
	int expectedStructSize();
	
	private void onUnexpectedStructSizeThenThrowException(int expectedSize, String sqlTypeName, Object[] attributes) {
		if (attributes == null || attributes.length != expectedSize) {
			String msg = buildDefaultMessage(expectedSize, sqlTypeName, attributes);
			throw new UnexpectedStructSizeException(expectedSize, sqlTypeName, attributes, msg);
		}
	}
	
	private String buildDefaultMessage(int expectedSize, String sqlTypeName, Object[] attributes) {
		StringBuilder sb = new StringBuilder();
		sb
			.append("Struct of type ").append(sqlTypeName).append(" should have ").append(expectedSize).append(" attributes but ");
		if (attributes == null) { // Null shouldn't happen but whatever
			sb.append("attributes array is null.");
		} else {
			sb.append("has ").append(attributes.length).append(". ")
			.append("Attributes values are : ");
			Stream.of(attributes).forEach(att -> sb.append("[").append(att).append("]"));
		}
		
		return sb.toString();
	}
}
