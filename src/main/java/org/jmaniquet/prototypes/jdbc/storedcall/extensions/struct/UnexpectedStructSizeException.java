package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import org.springframework.dao.DataRetrievalFailureException;

public class UnexpectedStructSizeException extends DataRetrievalFailureException {

	private static final long serialVersionUID = 9058228157030313723L;

	private int expectedSize;
	private String sqlTypeName;
	private Object[] attributes;
	
	public UnexpectedStructSizeException(int expectedSize, String sqlTypeName, Object[] attributes, String msg) {
		super(msg);
		this.expectedSize = expectedSize;
		this.sqlTypeName = sqlTypeName;
		this.attributes = attributes;
	}

	public int getExpectedSize() {
		return expectedSize;
	}

	public String getSqlTypeName() {
		return sqlTypeName;
	}

	public Object[] getAttributes() {
		return attributes;
	}
}
