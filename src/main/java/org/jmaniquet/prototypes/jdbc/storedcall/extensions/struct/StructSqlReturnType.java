package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Struct;

import org.springframework.jdbc.core.SqlReturnType;

class StructSqlReturnType<T> implements SqlReturnType {

	private StructMapper<T> mapper;
	
	StructSqlReturnType(StructMapper<T> mapper) {
		this.mapper = mapper;
	}

	@Override
	public Object getTypeValue(CallableStatement cs, int paramIndex, int sqlType, String typeName) throws SQLException {
		Struct struct = cs.getObject(paramIndex, Struct.class);
		T result = mapper.fromStruct(struct);
		return result;
	}
}
