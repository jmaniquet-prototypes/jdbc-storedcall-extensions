package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import java.sql.Types;

import org.springframework.jdbc.core.SqlParameter;

public class StructSqlParameter extends SqlParameter {

	public StructSqlParameter(String name, String typeName) {
		super(name, Types.STRUCT, typeName);
	}
}
