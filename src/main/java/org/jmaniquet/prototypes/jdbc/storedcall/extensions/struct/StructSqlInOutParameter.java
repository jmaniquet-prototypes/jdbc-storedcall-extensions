package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import java.sql.Types;

import org.springframework.jdbc.core.SqlInOutParameter;

public class StructSqlInOutParameter<T> extends SqlInOutParameter {

	public StructSqlInOutParameter(String name, String typeName, StructMapper<T> structMapper) {
		super(name, Types.STRUCT, typeName, new StructSqlReturnType<T>(structMapper));
	}
}
