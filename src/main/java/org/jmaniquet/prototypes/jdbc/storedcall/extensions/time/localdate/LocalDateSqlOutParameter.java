package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdate;

import java.sql.Types;

import org.springframework.jdbc.core.SqlOutParameter;

public class LocalDateSqlOutParameter extends SqlOutParameter {

	public LocalDateSqlOutParameter(String name) {
		super(name, Types.DATE, null, new LocalDateSqlReturnType());
	}
}
