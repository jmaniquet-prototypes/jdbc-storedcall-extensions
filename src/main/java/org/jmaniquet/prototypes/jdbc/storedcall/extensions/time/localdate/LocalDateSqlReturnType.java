package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdate;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.time.LocalDate;

import org.springframework.jdbc.core.SqlReturnType;

class LocalDateSqlReturnType implements SqlReturnType {

	@Override
	public Object getTypeValue(CallableStatement cs, int paramIndex, int sqlType, String typeName) throws SQLException {
		return cs.getObject(paramIndex, LocalDate.class);
	}
}
