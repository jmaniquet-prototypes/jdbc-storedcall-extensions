package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localtime;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.time.LocalTime;

import org.springframework.jdbc.core.SqlReturnType;

class LocalTimeSqlReturnType implements SqlReturnType {

	@Override
	public Object getTypeValue(CallableStatement cs, int paramIndex, int sqlType, String typeName) throws SQLException {
		return cs.getObject(paramIndex, LocalTime.class);
	}
}
