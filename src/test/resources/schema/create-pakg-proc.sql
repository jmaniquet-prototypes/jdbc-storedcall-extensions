create or replace package FOO_PAKG is
	procedure INSERT_FOO_1(P_FOO in FOO_TABLE.FOO%TYPE);
	procedure INSERT_FOO_2(P_FOO FOO_TABLE.FOO%TYPE);
	procedure SELECT_FOO_OUT(P_FOO out FOO_TABLE.FOO%TYPE);
	function SELECT_FOO_FUNC return FOO_TABLE.FOO%TYPE;
	procedure SELECT_FOO_IN_OUT(P_FOO in out FOO_TABLE.FOO%TYPE);
end;/;

create or replace package body FOO_PAKG is
	procedure INSERT_FOO_1(P_FOO in FOO_TABLE.FOO%TYPE) is 
	begin
		insert into FOO_TABLE (FOO) values (P_FOO);
	end;
	
	procedure INSERT_FOO_2(P_FOO FOO_TABLE.FOO%TYPE) is 
	begin
		insert into FOO_TABLE (FOO) values (P_FOO);
	end;
	
	procedure SELECT_FOO_OUT(P_FOO out FOO_TABLE.FOO%TYPE) is 
	begin
		select FOO into P_FOO from FOO_TABLE where rownum = 1;
	end;
	
	function SELECT_FOO_FUNC return FOO_TABLE.FOO%TYPE is
		temp FOO_TABLE.FOO%TYPE;
	begin
		select FOO into temp from FOO_TABLE where rownum = 1;
		return temp;
	end;
	
	procedure SELECT_FOO_IN_OUT(P_FOO in out FOO_TABLE.FOO%TYPE) is
		temp FOO_TABLE.FOO%TYPE;
	begin
		temp := P_FOO;
		select FOO into P_FOO from FOO_TABLE where rownum = 1;
		insert into FOO_TABLE (FOO) values (temp);
	end;
end;/;