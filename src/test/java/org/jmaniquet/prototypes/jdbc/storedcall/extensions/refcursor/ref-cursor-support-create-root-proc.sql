create or replace procedure SELECT_FOO_OUT(P_FOO out SYS_REFCURSOR) is
begin
	open P_FOO for select FOO from FOO_TABLE;
end;/;

create or replace function SELECT_FOO_FUNC return SYS_REFCURSOR is
	temp SYS_REFCURSOR;
begin
	open temp for select FOO from FOO_TABLE;
	return temp;
end;/;