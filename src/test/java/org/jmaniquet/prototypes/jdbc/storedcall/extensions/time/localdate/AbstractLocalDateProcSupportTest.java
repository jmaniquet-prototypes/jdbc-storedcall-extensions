package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.context.jdbc.SqlMergeMode.MergeMode.MERGE;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.jmaniquet.prototypes.jdbc.storedcall.extensions.provider.BeanProvider;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;

@Tag("local-date")
abstract class AbstractLocalDateProcSupportTest {

	@Autowired
	private BeanProvider config;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Test
	public void testInsertLocalDate1() {
		testInsertLocalDate(config.insert1Bean());
	}
	
	@Test
	public void testInsertLocalDate2() {
		testInsertLocalDate(config.insert2Bean());
	}
	
	private void testInsertLocalDate(SimpleJdbcCall call) {
		LocalDate expected = LocalDate.of(2016, 12, 27);
		
		Map<String, Object> resultMap = call.execute(expected);
		assertThat(resultMap).isEmpty();
		
		List<LocalDate> results = jdbcTemplate.queryForList("select FOO from FOO_TABLE", LocalDate.class);
		assertThat(results).hasOnlyOneElementSatisfying(result -> assertThat(result).isEqualTo(expected));
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_DATE('2007/05/03', 'yyyy/mm/dd'))"})
	@SqlMergeMode(MERGE)
	public void testSelectLocalDateOut() {
		testSelectLocalDate(config.selectOutBean(), "P_FOO");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_DATE('2007/05/03', 'yyyy/mm/dd'))"})
	@SqlMergeMode(MERGE)
	public void testSelectLocalDateFunc() {
		testSelectLocalDate(config.selectFuncBean(), "FUNC_RESULT");
	}

	private void testSelectLocalDate(SimpleJdbcCall call, String outKey) {
		LocalDate expected = LocalDate.of(2007, 5, 3);
		
		Map<String, Object> resultMap = call.execute();
		assertThat(resultMap)
			.hasEntrySatisfying(outKey, actual -> assertThat(actual).isInstanceOf(LocalDate.class).isEqualTo(expected))
			.hasSize(1);
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_DATE('2019/04/01', 'yyyy/mm/dd'))"})
	@SqlMergeMode(MERGE)
	public void testSelectLocalDateInOut() {
		LocalDate willBeInserted = LocalDate.of(2019, 10, 21);
		LocalDate alreadyExists = LocalDate.of(2019, 4, 1);
		
		Map<String, Object> resultMap = config.selectInOutBean().execute(willBeInserted);
		assertThat(resultMap)
			.hasEntrySatisfying("P_FOO", actual -> assertThat(actual).isInstanceOf(LocalDate.class).isEqualTo(alreadyExists))
			.hasSize(1);
		
		List<LocalDate> results = jdbcTemplate.queryForList("select FOO from FOO_TABLE", LocalDate.class);
		assertThat(results)
			.containsExactlyInAnyOrder(willBeInserted, alreadyExists)
			.hasSize(2);
	}
}
