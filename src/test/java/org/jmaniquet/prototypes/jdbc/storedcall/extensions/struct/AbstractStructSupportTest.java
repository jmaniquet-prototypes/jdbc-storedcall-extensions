package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.springframework.test.context.jdbc.SqlMergeMode.MergeMode.MERGE;

import java.util.List;
import java.util.Map;

import org.jmaniquet.prototypes.jdbc.storedcall.extensions.provider.BeanProvider;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;

@Tag("struct")
abstract class AbstractStructSupportTest {
	
	@Autowired
	private BeanProvider config;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Test
	public void testInsertStruct1() {
		testInsertStruct(config.insert1Bean());
	}
	
	@Test
	public void testInsertStruct2() {
		testInsertStruct(config.insert2Bean());
	}
	
	private void testInsertStruct(SimpleJdbcCall call) {
		FooHolder holder = new FooHolder("dummy1", "dummy2");
		
		Map<String, Object> resultMap = call.execute(new StructSqlValue<FooHolder>(holder, new FooHolderStructMapper()));
		assertThat(resultMap).isEmpty();
		
		List<Map<String, Object>> results = jdbcTemplate.queryForList("select FOO_1, FOO_2 from FOO_TABLE");
		assertThat(results).hasOnlyOneElementSatisfying(
				row -> assertThat(row)
					.containsEntry("FOO_1", "dummy1")
					.containsEntry("FOO_2", "dummy2")
					.hasSize(2));
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO_1, FOO_2) values ('dummy1', 'dummy2')"})
	@SqlMergeMode(MERGE)
	public void testSelectStructOut() {
		testSelectStruct(config.selectOutBean(), "P_FOO");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO_1, FOO_2) values ('dummy1', 'dummy2')"})
	@SqlMergeMode(MERGE)
	public void testSelectStructFunc() {
		testSelectStruct(config.selectFuncBean(), "FUNC_RESULT");
	}
	
	private void testSelectStruct(SimpleJdbcCall call, String outKey) {
		Map<String, Object> resultMap = call.execute();
		assertThat(resultMap)
			.hasEntrySatisfying(outKey, actual -> assertThat(actual).isNotNull().isInstanceOf(FooHolder.class))
			.hasSize(1);
		
		FooHolder actual = (FooHolder) resultMap.get(outKey);
		assertSoftly(softly -> {
			softly.assertThat(actual.getFoo1()).isEqualTo("dummy1");
			softly.assertThat(actual.getFoo2()).isEqualTo("dummy2");
		});
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO_1, FOO_2) values ('dummy1', 'dummy2')"})
	@SqlMergeMode(MERGE)
	public void testSelectStructInOut() {
		FooHolder willBeInserted = new FooHolder("dummy3", "dummy4");

		Map<String, Object> resultMap = config.selectInOutBean().execute(new StructSqlValue<FooHolder>(willBeInserted, new FooHolderStructMapper()));
		assertThat(resultMap)
			.hasEntrySatisfying("P_FOO", actual -> assertThat(actual).isNotNull().isInstanceOf(FooHolder.class))
			.hasSize(1);
		
		FooHolder actual = (FooHolder) resultMap.get("P_FOO");
		assertSoftly(softly -> {
			softly.assertThat(actual.getFoo1()).isEqualTo("dummy1");
			softly.assertThat(actual.getFoo2()).isEqualTo("dummy2");
		});
		
		List<Map<String, Object>> results = jdbcTemplate.queryForList("select FOO_1, FOO_2 from FOO_TABLE");
		assertThat(results).anySatisfy(
				row -> assertThat(row)
					.containsEntry("FOO_1", "dummy1")
					.containsEntry("FOO_2", "dummy2")
					.hasSize(2));
		assertThat(results).anySatisfy(
				row -> assertThat(row)
					.containsEntry("FOO_1", "dummy3")
					.containsEntry("FOO_2", "dummy4")
					.hasSize(2));
		assertThat(results).hasSize(2);
	}
}
