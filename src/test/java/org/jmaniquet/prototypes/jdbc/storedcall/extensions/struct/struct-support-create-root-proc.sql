create or replace procedure INSERT_FOO_1(P_FOO in FOO_HOLDER) is
begin
	insert into FOO_TABLE (FOO_1, FOO_2) values (P_FOO.FOO_1, P_FOO.FOO_2);
end;/;

create or replace procedure INSERT_FOO_2(P_FOO FOO_HOLDER) is
begin
	insert into FOO_TABLE (FOO_1, FOO_2) values (P_FOO.FOO_1, P_FOO.FOO_2);
end;/;

create or replace procedure SELECT_FOO_OUT(P_FOO out FOO_HOLDER) is
	temp1 FOO_TABLE.FOO_1%TYPE;
	temp2 FOO_TABLE.FOO_2%TYPE;
begin
	select FOO_1, FOO_2 into temp1, temp2 from FOO_TABLE where rownum = 1;
	P_FOO := FOO_HOLDER(temp1, temp2);
end;/;

create or replace function SELECT_FOO_FUNC return FOO_HOLDER is
	temp1 FOO_TABLE.FOO_1%TYPE;
	temp2 FOO_TABLE.FOO_2%TYPE;
	temp3 FOO_HOLDER;
begin
	select FOO_1, FOO_2 into temp1, temp2 from FOO_TABLE where rownum = 1;
	temp3 := FOO_HOLDER(temp1, temp2);
	return temp3;
end;/;

create or replace procedure SELECT_FOO_IN_OUT(P_FOO in out FOO_HOLDER) is
	temp1 FOO_TABLE.FOO_1%TYPE;
	temp2 FOO_TABLE.FOO_2%TYPE;
begin
	select FOO_1, FOO_2 into temp1, temp2 from FOO_TABLE where rownum = 1;
	insert into FOO_TABLE (FOO_1, FOO_2) values (P_FOO.FOO_1, P_FOO.FOO_2);
	P_FOO := FOO_HOLDER(temp1, temp2);
end;/;