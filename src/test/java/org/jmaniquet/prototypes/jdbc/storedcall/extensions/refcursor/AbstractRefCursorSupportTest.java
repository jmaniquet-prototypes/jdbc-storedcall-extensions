package org.jmaniquet.prototypes.jdbc.storedcall.extensions.refcursor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.context.jdbc.SqlMergeMode.MergeMode.MERGE;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;

@Tag("ref-cursor")
abstract class AbstractRefCursorSupportTest {

	@Autowired
	private RefCursorBeanProvider config;
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values ('dummy1')", "insert into FOO_TABLE (FOO) values ('dummy2')"})
	@SqlMergeMode(MERGE)
	public void testSelectRefCursorOut1() {
		testSelectRefCursor(config.selectOutBean1(), "P_FOO");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values ('dummy1')", "insert into FOO_TABLE (FOO) values ('dummy2')"})
	@SqlMergeMode(MERGE)
	public void testSelectRefCursorOut2() {
		testSelectRefCursor(config.selectOutBean2(), "P_FOO");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values ('dummy1')", "insert into FOO_TABLE (FOO) values ('dummy2')"})
	@SqlMergeMode(MERGE)
	public void testSelectRefCursorFunc1() {
		testSelectRefCursor(config.selectFuncBean1(), "FUNC_RESULT");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values ('dummy1')", "insert into FOO_TABLE (FOO) values ('dummy2')"})
	@SqlMergeMode(MERGE)
	public void testSelectRefCursorFunc2() {
		testSelectRefCursor(config.selectFuncBean2(), "FUNC_RESULT");
	}

	private void testSelectRefCursor(SimpleJdbcCall call, String outKey) {
		Map<String, Object> resultMap = call.execute();
		assertThat(resultMap)
			.hasEntrySatisfying(outKey, actual -> assertThat(actual).isNotNull().isInstanceOf(List.class))
			.hasSize(1);
		
		@SuppressWarnings("unchecked")
		List<String> procResults = (List<String>) resultMap.get(outKey);
		assertThat(procResults).containsExactlyInAnyOrder("dummy1", "dummy2");
	}
}
