package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

public class FooHolder {

	private String foo1;
	private String foo2;
	
	public FooHolder(String foo1, String foo2) {
		super();
		this.foo1 = foo1;
		this.foo2 = foo2;
	}
	
	public String getFoo1() {
		return foo1;
	}
	public String getFoo2() {
		return foo2;
	}
	public void setFoo1(String foo1) {
		this.foo1 = foo1;
	}
	public void setFoo2(String foo2) {
		this.foo2 = foo2;
	}
}
