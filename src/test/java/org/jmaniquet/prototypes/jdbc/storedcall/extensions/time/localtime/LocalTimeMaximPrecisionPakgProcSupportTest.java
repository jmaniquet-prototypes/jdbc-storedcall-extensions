package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localtime;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

@SpringBootTest(classes = LocalTimePakgProcSupportTestConfig.class)
@Sql(scripts = "local-time-support-create-table-maxim-precision.sql")
@Sql(scripts = "classpath:schema/create-pakg-proc.sql", config = @SqlConfig(separator = "/;"))
@Sql(scripts = "classpath:schema/drop-table.sql", 		executionPhase = AFTER_TEST_METHOD)
public class LocalTimeMaximPrecisionPakgProcSupportTest extends AbstractLocalTimeMaximPrecisionProcSupportTest {
}
