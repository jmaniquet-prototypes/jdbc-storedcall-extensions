package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.offsetdatetime;

import org.jmaniquet.prototypes.jdbc.storedcall.extensions.provider.BeanProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Configuration
@EnableAutoConfiguration
class OffsetDateTimePakgProcSupportTestConfig implements BeanProvider {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Bean
	public SimpleJdbcCall insert1Bean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName("FOO_PAKG")
				.withProcedureName("INSERT_FOO_1")
				.declareParameters(new OffsetDateTimeSqlParameter("P_FOO"));
	}

	@Bean
	public SimpleJdbcCall insert2Bean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName("FOO_PAKG")
				.withProcedureName("INSERT_FOO_2")
				.declareParameters(new OffsetDateTimeSqlParameter("P_FOO"));
	}

	@Bean
	public SimpleJdbcCall selectOutBean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName("FOO_PAKG")
				.withProcedureName("SELECT_FOO_OUT")
				.declareParameters(new OffsetDateTimeSqlOutParameter("P_FOO"));
	}

	@Bean
	public SimpleJdbcCall selectFuncBean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName("FOO_PAKG")
				.withFunctionName("SELECT_FOO_FUNC")
				.declareParameters(new OffsetDateTimeSqlOutParameter("FUNC_RESULT"));
	}

	@Bean
	public SimpleJdbcCall selectInOutBean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName("FOO_PAKG")
				.withProcedureName("SELECT_FOO_IN_OUT")
				.declareParameters(new OffsetDateTimeSqlInOutParameter("P_FOO"));
	}
}
