package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

@SpringBootTest(classes = StructPakgProcSupportTestConfig.class)
@Sql(scripts = "struct-support-create-table.sql")
@Sql(scripts = "struct-support-create-pakg-proc.sql", 	config = @SqlConfig(separator = "/;"))
@Sql(scripts = "classpath:schema/drop-table.sql", 		executionPhase = AFTER_TEST_METHOD)
public class StructPakgProcSupportTest extends AbstractStructSupportTest {
}
