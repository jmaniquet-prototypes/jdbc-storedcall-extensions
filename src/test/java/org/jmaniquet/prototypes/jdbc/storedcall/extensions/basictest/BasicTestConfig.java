package org.jmaniquet.prototypes.jdbc.storedcall.extensions.basictest;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
public class BasicTestConfig {
}
