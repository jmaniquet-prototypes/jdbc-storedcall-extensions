package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdatetime;

import org.jmaniquet.prototypes.jdbc.storedcall.extensions.provider.BeanProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Configuration
@EnableAutoConfiguration
class LocalDateTimeRootProcSupportTestConfig implements BeanProvider {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Bean
	public SimpleJdbcCall insert1Bean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("INSERT_FOO_1")
				.declareParameters(new LocalDateTimeSqlParameter("P_FOO"));
	}

	@Bean
	public SimpleJdbcCall insert2Bean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("INSERT_FOO_2")
				.declareParameters(new LocalDateTimeSqlParameter("P_FOO"));
	}

	@Bean
	public SimpleJdbcCall selectOutBean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("SELECT_FOO_OUT")
				.declareParameters(new LocalDateTimeSqlOutParameter("P_FOO"));
	}

	@Bean
	public SimpleJdbcCall selectFuncBean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withFunctionName("SELECT_FOO_FUNC")
				.declareParameters(new LocalDateTimeSqlOutParameter("FUNC_RESULT"));
	}

	@Bean
	public SimpleJdbcCall selectInOutBean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("SELECT_FOO_IN_OUT")
				.declareParameters(new LocalDateTimeSqlInOutParameter("P_FOO"));
	}
}
