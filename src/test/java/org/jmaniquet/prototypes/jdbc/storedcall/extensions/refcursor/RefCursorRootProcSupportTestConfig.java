package org.jmaniquet.prototypes.jdbc.storedcall.extensions.refcursor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Configuration
@EnableAutoConfiguration
class RefCursorRootProcSupportTestConfig implements RefCursorBeanProvider {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Bean
	public SimpleJdbcCall selectOutBean1() {
		SingleColumnRowMapper<String> rowMapper = new SingleColumnRowMapper<>(String.class);
		RefCursorSqlOutParameter<String> outParam = new RefCursorSqlOutParameter<>("P_FOO", rowMapper);
		return new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("SELECT_FOO_OUT")
				.declareParameters(outParam);
	}
	
	@Bean
	public SimpleJdbcCall selectOutBean2() {
		SingleColumnRowMapper<String> rowMapper = new SingleColumnRowMapper<>(String.class);
		return new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("SELECT_FOO_OUT")
				.returningResultSet("P_FOO", rowMapper);
	}
	
	@Bean
	public SimpleJdbcCall selectFuncBean1() {
		SingleColumnRowMapper<String> rowMapper = new SingleColumnRowMapper<>(String.class);
		RefCursorSqlOutParameter<String> outParam = new RefCursorSqlOutParameter<>("FUNC_RESULT", rowMapper);
		return new SimpleJdbcCall(jdbcTemplate)
				.withFunctionName("SELECT_FOO_FUNC")
				.declareParameters(outParam);
	}
	
	@Bean
	public SimpleJdbcCall selectFuncBean2() {
		SingleColumnRowMapper<String> rowMapper = new SingleColumnRowMapper<>(String.class);
		return new SimpleJdbcCall(jdbcTemplate)
				.withFunctionName("SELECT_FOO_FUNC")
				.returningResultSet("FUNC_RESULT", rowMapper);
	}
}
