create table FOO_TABLE (
	FOO varchar2(100)
);

create or replace type FOO_ARRAY as varray(5) of varchar2(100);