package org.jmaniquet.prototypes.jdbc.storedcall.extensions.basictest;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestExecutionListeners.MergeMode;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest(classes = BasicTestConfig.class)
@TestExecutionListeners(
		listeners = BasicTestTestExecutionListener.class,
		mergeMode = MergeMode.MERGE_WITH_DEFAULTS)
public class BasicTest {
	
	private static final Logger logger = LoggerFactory.getLogger(BasicTest.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Test
	public void testStaticIntFromDual() {
		Integer result = jdbcTemplate.queryForObject("select 1 from dual", int.class);
		assertThat(result).isEqualTo(1);
	}
	
	@Test
	public void testSysTimestampFromDualAsLocalDateTime() {
		testSysTimestampFromDual(LocalDateTime.class);
	}

	@Test
	public void testSysTimestampFromDualAsTimestamp() {
		testSysTimestampFromDual(Timestamp.class);
	}
	
	@Test
	public void testSysTimestampFromDualAsZonedDateTime() {
		testSysTimestampFromDual(ZonedDateTime.class);
	}

	private <T> T testSysTimestampFromDual(Class<T> requiredType) {
		T result = jdbcTemplate.queryForObject("select SYSTIMESTAMP from dual", requiredType);
		assertThat(result).isNotNull();
		logger.info("{} from oracle : {}", requiredType, result);
		return result;
	}
	
	@Test
	@Sql("basic-test.sql")
	public void testBasicData() {
		List<String> result = jdbcTemplate.queryForList("select FOO from DUMMY", String.class);
		assertThat(result).containsExactlyInAnyOrder("bar-1", "bar-2");
	}
}
