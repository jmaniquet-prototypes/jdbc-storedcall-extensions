create or replace package FOO_PAKG is
	procedure SELECT_FOO_OUT(P_FOO out SYS_REFCURSOR);
	function SELECT_FOO_FUNC return SYS_REFCURSOR;
end;/;

create or replace package body FOO_PAKG is
	procedure SELECT_FOO_OUT(P_FOO out SYS_REFCURSOR) is
	begin
		open P_FOO for select FOO from FOO_TABLE;
	end;
	
	function SELECT_FOO_FUNC return SYS_REFCURSOR is
		temp SYS_REFCURSOR;
	begin
		open temp for select FOO from FOO_TABLE;
		return temp;
	end;
end;/;