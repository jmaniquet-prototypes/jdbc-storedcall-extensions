package org.jmaniquet.prototypes.jdbc.storedcall.extensions.array;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.context.jdbc.SqlMergeMode.MergeMode.MERGE;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.jmaniquet.prototypes.jdbc.storedcall.extensions.oracle.OracleArrayCreatorCallback;
import org.jmaniquet.prototypes.jdbc.storedcall.extensions.provider.BeanProvider;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;

@Tag("array")
abstract class AbstractArraySupportTest {

	@Autowired
	private BeanProvider config;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Test
	public void testInsertArray1() {
		testInsertArray(config.insert1Bean(), List.of("dummy1", "dummy2"));
	}
	
	@Test
	public void testInsertArray2() {
		testInsertArray(config.insert2Bean(), List.of("dummy1", "dummy2"));
	}
	
	private void testInsertArray(SimpleJdbcCall call, Collection<String> dummies) {
		Map<String, Object> resultMap = call.execute(new ArraySqlValue<>("FOO_ARRAY", dummies).withArrayCreatorCallback(new OracleArrayCreatorCallback()));
		assertThat(resultMap).isEmpty();
		
		List<String> results = jdbcTemplate.queryForList("select FOO from FOO_TABLE", String.class);
		assertThat(results).containsExactlyInAnyOrder("dummy1", "dummy2");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values ('dummy1')", "insert into FOO_TABLE (FOO) values ('dummy2')"})
	@SqlMergeMode(MERGE)
	public void testSelectArrayOut() {
		testSelectArray(config.selectOutBean(), "P_FOO");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values ('dummy1')", "insert into FOO_TABLE (FOO) values ('dummy2')"})
	@SqlMergeMode(MERGE)
	public void testSelectArrayFunc() {
		testSelectArray(config.selectFuncBean(), "FUNC_RESULT");
	}
	
	private void testSelectArray(SimpleJdbcCall call, String outKey) {
		Map<String, Object> resultMap = call.execute();
		assertThat(resultMap)
			.hasEntrySatisfying(outKey, actual -> assertThat(actual).isNotNull().isInstanceOf(List.class))
			.hasSize(1);
		
		@SuppressWarnings("unchecked")
		List<String> actual = (List<String>) resultMap.get(outKey);
		assertThat(actual).containsExactlyInAnyOrder("dummy1", "dummy2");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values ('dummy1')", "insert into FOO_TABLE (FOO) values ('dummy2')"})
	@SqlMergeMode(MERGE)
	public void testSelectArrayInOut() {
		List<String> dummies = List.of("dummy3", "dummy4");
		
		Map<String, Object> resultMap = config.selectInOutBean().execute(new ArraySqlValue<>("FOO_ARRAY", dummies).withArrayCreatorCallback(new OracleArrayCreatorCallback()));
		assertThat(resultMap)
			.hasEntrySatisfying("P_FOO", actual -> assertThat(actual).isNotNull().isInstanceOf(List.class))
			.hasSize(1);
		
		@SuppressWarnings("unchecked")
		List<String> procResults = (List<String>) resultMap.get("P_FOO");
		assertThat(procResults).containsExactlyInAnyOrder("dummy1", "dummy2");
		
		List<String> selectResults = jdbcTemplate.queryForList("select FOO from FOO_TABLE", String.class);
		assertThat(selectResults).containsExactlyInAnyOrder("dummy1", "dummy2", "dummy3", "dummy4");
	}
}
