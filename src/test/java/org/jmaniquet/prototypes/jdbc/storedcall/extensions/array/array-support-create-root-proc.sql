create or replace procedure INSERT_FOO_1(P_FOO in FOO_ARRAY) is
begin
	for i IN 1..P_FOO.count loop
		insert into FOO_TABLE (FOO) values (P_FOO(i));
	end loop;
end;/;

create or replace procedure INSERT_FOO_2(P_FOO FOO_ARRAY) is
begin
	for i IN 1..P_FOO.count loop
		insert into FOO_TABLE (FOO) values (P_FOO(i));
	end loop;
end;/;

create or replace procedure SELECT_FOO_OUT(P_FOO out FOO_ARRAY) is
	cursor FOOS is select FOO from FOO_TABLE;
	counter integer := 0;
begin
	P_FOO := FOO_ARRAY();
	for n in FOOS loop
		counter := counter + 1;
		P_FOO.extend;
		P_FOO(counter) := n.FOO;
	end loop;
end;/;

create or replace function SELECT_FOO_FUNC return FOO_ARRAY is
	cursor FOOS is select FOO from FOO_TABLE;
	counter integer := 0;
	temp FOO_ARRAY := FOO_ARRAY();
begin
	for n in FOOS loop
		counter := counter + 1;
		temp.extend;
		temp(counter) := n.FOO;
	end loop;
	return temp;
end;/;

create or replace procedure SELECT_FOO_IN_OUT(P_FOO in out FOO_ARRAY) is
	cursor FOOS is select FOO from FOO_TABLE;
	counter integer := 0;
	temp FOO_ARRAY;
begin
	temp := P_FOO;
	P_FOO := FOO_ARRAY();
	for n in FOOS loop
		counter := counter + 1;
		P_FOO.extend;
		P_FOO(counter) := n.FOO;
	end loop;
	for i IN 1..temp.count loop
		insert into FOO_TABLE (FOO) values (temp(i));
	end loop;
end;/;