package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdate;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

@SpringBootTest(classes = LocalDatePakgProcSupportTestConfig.class)
@Sql(scripts = "local-date-support-create-table.sql")
@Sql(scripts = "classpath:schema/create-pakg-proc.sql", config = @SqlConfig(separator = "/;"))
@Sql(scripts = "classpath:schema/drop-table.sql", 		executionPhase = AFTER_TEST_METHOD)
public class LocalDatePakgProcSupportTest extends AbstractLocalDateProcSupportTest {
}
