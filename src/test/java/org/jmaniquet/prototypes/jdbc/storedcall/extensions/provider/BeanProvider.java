package org.jmaniquet.prototypes.jdbc.storedcall.extensions.provider;

import org.springframework.jdbc.core.simple.SimpleJdbcCall;

public interface BeanProvider {
	
	SimpleJdbcCall insert1Bean();
	
	SimpleJdbcCall insert2Bean();
	
	SimpleJdbcCall selectOutBean();
	
	SimpleJdbcCall selectFuncBean();
	
	SimpleJdbcCall selectInOutBean();
}
