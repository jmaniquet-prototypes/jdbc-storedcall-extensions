package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localtime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.springframework.test.context.jdbc.SqlMergeMode.MergeMode.MERGE;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;

import org.jmaniquet.prototypes.jdbc.storedcall.extensions.provider.BeanProvider;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;

@Tag("local-time")
abstract class AbstractLocalTimeMaximPrecisionProcSupportTest {

	@Autowired
	private BeanProvider config;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Test
	public void testInsertLocalTime1() {
		testInsertLocalTime(config.insert1Bean());
	}
	
	@Test
	public void testInsertLocalTime2() {
		testInsertLocalTime(config.insert2Bean());
	}
	
	private void testInsertLocalTime(SimpleJdbcCall call) {
		LocalTime expected = LocalTime.of(13, 12, 56, 537_456_123);
		
		Map<String, Object> resultMap = call.execute(expected);
		assertThat(resultMap).isEmpty();
		
		List<LocalTime> results = jdbcTemplate.queryForList("select FOO from FOO_TABLE", LocalTime.class);
		assertThat(results).hasOnlyOneElementSatisfying(result -> assertThat(result).isEqualTo(expected));
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_TIMESTAMP('15:32 14.537456298', 'HH24:MI SS.FF'))"})
	@SqlMergeMode(MERGE)
	public void testSelectLocalTimeOut() {
		testSelectLocalTime(config.selectOutBean(), "P_FOO");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_TIMESTAMP('15:32 14.537456298', 'HH24:MI SS.FF'))"})
	@SqlMergeMode(MERGE)
	public void testSelectLocalTimeFunc() {
		testSelectLocalTime(config.selectFuncBean(), "FUNC_RESULT");
	}
	
	private void testSelectLocalTime(SimpleJdbcCall call, String outKey) {
		LocalTime expected = LocalTime.of(15, 32, 14, 537_456_298);
		
		Map<String, Object> resultMap = call.execute();
		assertThat(resultMap)
			.hasEntrySatisfying(outKey, actual -> assertThat(actual).isInstanceOf(LocalTime.class).isEqualTo(expected))
			.hasSize(1);
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_TIMESTAMP('17:27 45.127272713', 'HH24:MI SS.FF'))"})
	@SqlMergeMode(MERGE)
	public void testSelectLocalTimeInOut() {
		LocalTime willBeInserted = LocalTime.of(21, 17, 23, 678_987_456);
		LocalTime alreadyExists = LocalTime.of(17, 27, 45, 127_272_713);
		
		Map<String, Object> resultMap = config.selectInOutBean().execute(willBeInserted);
		List<LocalTime> results = jdbcTemplate.queryForList("select FOO from FOO_TABLE", LocalTime.class);
		
		assertSoftly(softly -> {
			softly.assertThat(resultMap)
				.hasEntrySatisfying("P_FOO", actual -> assertThat(actual).isInstanceOf(LocalTime.class).isEqualTo(alreadyExists))
				.hasSize(1);
			
			softly.assertThat(results)
				.containsExactlyInAnyOrder(willBeInserted, alreadyExists)
				.hasSize(2);
		});
		
		
	}
}
