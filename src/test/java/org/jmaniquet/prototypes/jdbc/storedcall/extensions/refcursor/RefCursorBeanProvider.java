package org.jmaniquet.prototypes.jdbc.storedcall.extensions.refcursor;

import org.springframework.jdbc.core.simple.SimpleJdbcCall;

interface RefCursorBeanProvider {

	SimpleJdbcCall selectOutBean1();
	
	SimpleJdbcCall selectOutBean2();
	
	SimpleJdbcCall selectFuncBean1();
	
	SimpleJdbcCall selectFuncBean2();
}
