package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.offsetdatetime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.context.jdbc.SqlMergeMode.MergeMode.MERGE;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;

import org.jmaniquet.prototypes.jdbc.storedcall.extensions.provider.BeanProvider;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;

@Tag("offset-date-time")
abstract class AbstractOffsetDateTimeMaximPrecisionProcSupportTest {

	@Autowired
	private BeanProvider config;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Test
	public void testInsertOffsetDateTime1() {
		testInsertOffsetDateTime(config.insert1Bean());
	}
	
	@Test
	public void testInsertOffsetDateTime2() {
		testInsertOffsetDateTime(config.insert2Bean());
	}
	
	private void testInsertOffsetDateTime(SimpleJdbcCall call) {
		OffsetDateTime expected = OffsetDateTime.of(2016, 12, 27, 13, 12, 56, 537_456_123, ZoneOffset.of("+07:00"));
		
		Map<String, Object> resultMap = call.execute(expected);
		assertThat(resultMap).isEmpty();
		
		List<OffsetDateTime> results = jdbcTemplate.queryForList("select FOO from FOO_TABLE", OffsetDateTime.class);
		assertThat(results).hasOnlyOneElementSatisfying(result -> assertThat(result).isEqualTo(expected));
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_TIMESTAMP_TZ('2007/05/03 15:32 14.537456298 +7:00', 'yyyy/mm/dd HH24:MI SS.FF TZH:TZM'))"})
	@SqlMergeMode(MERGE)
	public void testSelectOffsetDateTimeOut() {
		testSelectOffsetDateTime(config.selectOutBean(), "P_FOO");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_TIMESTAMP_TZ('2007/05/03 15:32 14.537456298 +7:00', 'yyyy/mm/dd HH24:MI SS.FF TZH:TZM'))"})
	@SqlMergeMode(MERGE)
	public void testSelectOffsetDateTimeFunc() {
		testSelectOffsetDateTime(config.selectFuncBean(), "FUNC_RESULT");
	}
	
	private void testSelectOffsetDateTime(SimpleJdbcCall call, String outKey) {
		OffsetDateTime expected = OffsetDateTime.of(2007, 5, 3, 15, 32, 14, 537_456_298, ZoneOffset.of("+07:00"));
		
		Map<String, Object> resultMap = call.execute();
		assertThat(resultMap)
			.hasEntrySatisfying(outKey, actual -> assertThat(actual).isInstanceOf(OffsetDateTime.class).isEqualTo(expected))
			.hasSize(1);
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_TIMESTAMP_TZ('2019/04/01 17:27 45.127272713 +7:00', 'yyyy/mm/dd HH24:MI SS.FF TZH:TZM'))"})
	@SqlMergeMode(MERGE)
	public void testSelectOffsetDateTimeInOut() {
		OffsetDateTime willBeInserted = OffsetDateTime.of(2019, 10, 21, 21, 17, 23, 678_987_456, ZoneOffset.of("+08:00"));
		OffsetDateTime alreadyExists = OffsetDateTime.of(2019, 4, 1, 17, 27, 45, 127_272_713, ZoneOffset.of("+07:00"));
		
		Map<String, Object> resultMap = config.selectInOutBean().execute(willBeInserted);
		assertThat(resultMap)
			.hasEntrySatisfying("P_FOO", actual -> assertThat(actual).isInstanceOf(OffsetDateTime.class).isEqualTo(alreadyExists))
			.hasSize(1);
		
		List<OffsetDateTime> results = jdbcTemplate.queryForList("select FOO from FOO_TABLE", OffsetDateTime.class);
		assertThat(results)
			.containsExactlyInAnyOrder(willBeInserted, alreadyExists)
			.hasSize(2);
	}
}
