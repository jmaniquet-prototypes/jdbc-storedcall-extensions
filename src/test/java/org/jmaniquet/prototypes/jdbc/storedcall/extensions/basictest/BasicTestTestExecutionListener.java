package org.jmaniquet.prototypes.jdbc.storedcall.extensions.basictest;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListener;

public class BasicTestTestExecutionListener implements TestExecutionListener {
	
	private static final Logger logger = LoggerFactory.getLogger(BasicTestTestExecutionListener.class);
	
	@Override
	public void beforeTestClass(TestContext testContext) throws Exception {
		logger.info("Timestamp from jvm        : {}", new Timestamp(new Date().getTime()));
		logger.info("LocalDateTime from jvm    : {}", LocalDateTime.now());
		logger.info("ZonedDateTime from jvm    : {}", ZonedDateTime.now());
		logger.info("ZoneId from jvm           : {}", ZoneId.systemDefault());
		logger.info("TimeZone from java.util   : {}", TimeZone.getDefault());
		logger.info("TimeZone from system prop : {}", System.getProperty("user.timezone"));
		
		ApplicationContext ctx = testContext.getApplicationContext();
		
		Environment env = ctx.getEnvironment();
		String property = env.getProperty("user.timezone");
		logger.info("TimeZone from spring prop : {}", property);
		
		JdbcTemplate jdbcTemplate = ctx.getBean(JdbcTemplate.class);
		List<String> oracleTz = jdbcTemplate.queryForList("select distinct TZNAME from V$TIMEZONE_NAMES order by TZNAME", String.class);
		logger.debug("TimeZone from oracle :      {}", oracleTz);
	}
}
