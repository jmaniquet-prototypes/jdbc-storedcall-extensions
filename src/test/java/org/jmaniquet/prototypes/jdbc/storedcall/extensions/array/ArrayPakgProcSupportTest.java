package org.jmaniquet.prototypes.jdbc.storedcall.extensions.array;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

@SpringBootTest(classes = ArrayPakgProcSupportTestConfig.class)
@Sql(scripts = "array-support-create-table.sql")
@Sql(scripts = "array-support-create-pakg-proc.sql", 	config = @SqlConfig(separator = "/;"))
@Sql(scripts = "classpath:schema/drop-table.sql", 		executionPhase = AFTER_TEST_METHOD)
public class ArrayPakgProcSupportTest extends AbstractArraySupportTest {
}
