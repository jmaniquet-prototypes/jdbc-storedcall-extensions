package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localdatetime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.context.jdbc.SqlMergeMode.MergeMode.MERGE;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.jmaniquet.prototypes.jdbc.storedcall.extensions.provider.BeanProvider;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;

@Tag("local-date-time")
abstract class AbstractLocalDateTimeMaximPrecisionProcSupportTest {

	@Autowired
	private BeanProvider config;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Test
	public void testInsertLocalDateTime1() {
		testInsertLocalDateTime(config.insert1Bean());
	}
	
	@Test
	public void testInsertLocalDateTime2() {
		testInsertLocalDateTime(config.insert2Bean());
	}
	
	private void testInsertLocalDateTime(SimpleJdbcCall call) {
		LocalDateTime expected = LocalDateTime.of(2016, 12, 27, 13, 12, 56, 537_456_123);
		
		Map<String, Object> resultMap = call.execute(expected);
		assertThat(resultMap).isEmpty();
		
		List<LocalDateTime> results = jdbcTemplate.queryForList("select FOO from FOO_TABLE", LocalDateTime.class);
		assertThat(results).hasOnlyOneElementSatisfying(result -> assertThat(result).isEqualTo(expected));
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_TIMESTAMP('2007/05/03 15:32 14.537456298', 'yyyy/mm/dd HH24:MI SS.FF'))"})
	@SqlMergeMode(MERGE)
	public void testSelectLocalDateTimeOut() {
		testSelectLocalDateTime(config.selectOutBean(), "P_FOO");
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_TIMESTAMP('2007/05/03 15:32 14.537456298', 'yyyy/mm/dd HH24:MI SS.FF'))"})
	@SqlMergeMode(MERGE)
	public void testSelectLocalDateTimeFunc() {
		testSelectLocalDateTime(config.selectFuncBean(), "FUNC_RESULT");
	}
	
	private void testSelectLocalDateTime(SimpleJdbcCall call, String outKey) {
		LocalDateTime expected = LocalDateTime.of(2007, 5, 3, 15, 32, 14, 537_456_298);
		
		Map<String, Object> resultMap = call.execute();
		assertThat(resultMap)
			.hasEntrySatisfying(outKey, actual -> assertThat(actual).isInstanceOf(LocalDateTime.class).isEqualTo(expected))
			.hasSize(1);
	}
	
	@Test
	@Sql(statements = {"insert into FOO_TABLE (FOO) values (TO_TIMESTAMP('2019/04/01 17:27 45.127272713', 'yyyy/mm/dd HH24:MI SS.FF'))"})
	@SqlMergeMode(MERGE)
	public void testSelectLocalDateTimeInOut() {
		LocalDateTime willBeInserted = LocalDateTime.of(2019, 10, 21, 21, 17, 23, 678_987_456);
		LocalDateTime alreadyExists = LocalDateTime.of(2019, 4, 1, 17, 27, 45, 127_272_713);
		
		Map<String, Object> resultMap = config.selectInOutBean().execute(willBeInserted);
		assertThat(resultMap)
			.hasEntrySatisfying("P_FOO", actual -> assertThat(actual).isInstanceOf(LocalDateTime.class).isEqualTo(alreadyExists))
			.hasSize(1);
		
		List<LocalDateTime> results = jdbcTemplate.queryForList("select FOO from FOO_TABLE", LocalDateTime.class);
		assertThat(results)
			.containsExactlyInAnyOrder(willBeInserted, alreadyExists)
			.hasSize(2);
	}
}
