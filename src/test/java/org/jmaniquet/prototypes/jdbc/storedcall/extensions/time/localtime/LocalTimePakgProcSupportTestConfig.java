package org.jmaniquet.prototypes.jdbc.storedcall.extensions.time.localtime;

import org.jmaniquet.prototypes.jdbc.storedcall.extensions.provider.BeanProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Configuration
@EnableAutoConfiguration
class LocalTimePakgProcSupportTestConfig implements BeanProvider {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Bean
	public SimpleJdbcCall insert1Bean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName("FOO_PAKG")
				.withProcedureName("INSERT_FOO_1")
				.declareParameters(LocalTimeSqlParameter.forOracle("P_FOO"));
	}

	@Bean
	public SimpleJdbcCall insert2Bean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName("FOO_PAKG")
				.withProcedureName("INSERT_FOO_2")
				.declareParameters(LocalTimeSqlParameter.forOracle("P_FOO"));
	}

	@Bean
	public SimpleJdbcCall selectOutBean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName("FOO_PAKG")
				.withProcedureName("SELECT_FOO_OUT")
				.declareParameters(LocalTimeSqlOutParameter.forOracle("P_FOO"));
	}

	@Bean
	public SimpleJdbcCall selectFuncBean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName("FOO_PAKG")
				.withFunctionName("SELECT_FOO_FUNC")
				.declareParameters(LocalTimeSqlOutParameter.forOracle("FUNC_RESULT"));
	}

	@Bean
	public SimpleJdbcCall selectInOutBean() {
		return new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName("FOO_PAKG")
				.withProcedureName("SELECT_FOO_IN_OUT")
				.declareParameters(LocalTimeSqlInOutParameter.forOracle("P_FOO"));
	}
}
