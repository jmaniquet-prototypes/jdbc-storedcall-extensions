package org.jmaniquet.prototypes.jdbc.storedcall.extensions.refcursor;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

@SpringBootTest(classes = RefCursorPakgProcSupportTestConfig.class)
@Sql(scripts = "ref-cursor-support-create-table.sql")
@Sql(scripts = "ref-cursor-support-create-pakg-proc.sql", 	config = @SqlConfig(separator = "/;"))
@Sql(scripts = "classpath:schema/drop-table.sql", 			executionPhase = AFTER_TEST_METHOD)
public class RefCursorPakgProcSupportTest extends AbstractRefCursorSupportTest {

}