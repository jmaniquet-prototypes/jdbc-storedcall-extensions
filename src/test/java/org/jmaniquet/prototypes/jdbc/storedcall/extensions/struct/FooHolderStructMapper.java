package org.jmaniquet.prototypes.jdbc.storedcall.extensions.struct;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Struct;

public class FooHolderStructMapper implements StructAttributesMapper<FooHolder> {

	@Override
	public Struct toStruct(FooHolder source, Connection conn) throws SQLException {
		Object[] attributes = new Object[2];
		attributes[0] = source.getFoo1();
		attributes[1] = source.getFoo2();
		Struct struct = conn.createStruct("FOO_HOLDER", attributes);
		return struct;
	}

	@Override
	public FooHolder fromStructAttributes(Object[] attributes) throws SQLException {
		String foo1 = (String) attributes[0];
		String foo2 = (String) attributes[1];
		FooHolder result = new FooHolder(foo1, foo2);
		return result;
	}

	@Override
	public int expectedStructSize() {
		return 2;
	}
}
